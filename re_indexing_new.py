# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 14:09:24 2019

@author: mskauen
"""

# Automatisert reindexering av syslogger

from elasticsearch import Elasticsearch
from elasticsearch import client as c
import pandas as pd


es = Elasticsearch('https://query.elk.eikplatform.io:9200', ca_certs=False, 
                   verify_certs=False, http_auth=('mskauen','skau2779OP!'))
    

st = '-eik-apps-'
env_list = ['dev','prod','uat','test','qa']
y_list = ['2018','2019']
month_list = ["0"+str(m) for m in range(1,10)]+['10','11','12']


#%%

# Generate dataframe with total size and shards for all indexes in a month
# in a single environment (dev,prod, uat, test or qa) 


big_dict = {}
exceed_50 = {}

for env in env_list:
    env_st = env+st
    for y in y_list:
        env_y = env_st+y+'.'
        for m in month_list:
            name = env_y+m+"*"
            print(name)
            try:    
                list_ind = es.cat.indices(index = name, bytes = 'm')
                list_ind = list_ind.splitlines()
                
                if not list_ind:
                    continue
            except:
                continue
            tot_size = 0
            tot_shard = 0
            indices = []
            
            for row in list_ind:
                row = row.split(" ")
                row = list(filter(None, row))
                ind = row[2]
                tot_shard += int(row[5])
                tot_size += int(row[-2])
                indices.append(ind)
            
            big_dict[name] = [tot_size/1000, tot_shard, tot_size/(tot_shard*1000)]
            
            if tot_size > 50000:
                exceed_50[name] = [tot_size/1000, tot_shard, tot_size/(tot_shard*1000)]
        
    
df_size_tot = pd.DataFrame.from_dict(big_dict, orient='index',
                       columns = ['Total Size','Total Shards','Size per Shard'])

# Additional dataframe for large indices (>50GB in a month)
df_ex_50_tot = pd.DataFrame.from_dict(exceed_50, orient='index',
                       columns = ['Total Size','Total Shards','Size per Shard'])
    

#%%
# Print dataframe
print(df_size_tot)

#print(df_ex_50_tot)


#%%

# Function to do reindex job with 4 arguments

def ilm_func(template_name, rollover_alias, index_pattern):
    # Define new template
    b = {
      "index_patterns": [
        index_pattern 
      ],
      "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 1,
        "index": {
          "lifecycle": {
            "name": template_name, 
            "rollover_alias": rollover_alias 
          }
        }
      },
      "mappings": {
        "properties": {
          "message": {
            "type": "text"
          },
          "@timestamp": {
            "type": "date"
          }
        }
      }
    }
    
    t = c.IndicesClient.put_template(name = template_name, body = b)
    
    
    
    # Create first rollover index
    b2 = {
      "aliases": {
        rollover_alias:{
          "is_write_index": True 
        }
      }
    }
    
    first = es.index(index = index_pattern+'-0000001', body = b2)  
    
    a = 1
    return a

def post_reindex(old_indices, rollover_alias)
    
    # Post reindex from prod-eik-apps* to prod
    b3 = {
      "source": {
        "index": old_indices, 
        "sort": { "@timestamp": "desc" }
      },
      "dest": {
        "index": rollover_alias, 
        "op_type": "create" 
      }
    }
    
    re = es.reindex(b3)
    return 1


#%%

# Example, one reindex
call = ilm_func('prod', 'prod-roll', 'ilm-prod')
call2 = post_reindex('prod-eik-apps-2019.03*','prod-roll')

# Could do an if-test to check the size of old_indices (1st argument in post_reindex) 
# in the df_size_tot dataframe before reindexing

# Could make a similar for-loop structure to reindex all indices in elastic
# after calling ilm_func to get the template


# Example, reindex all old indices in prod environment
env_select = 'prod'
roll = env_select+'-roll'
ind_pattern = 'ilm-'+env_select

# First, define template and first rollover index
get_temp = ilm_func(env_select,roll,ind_pattern)

# Start posting for each month
for env in env_list:
    if env != env_select:
        continue
    env_st = env+st
    for y in y_list:
        env_y = env_st+y+'.'
        for m in month_list:
            name = env_y+m+"*"
            post = post_reindex(name,roll)





  